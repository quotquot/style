/*
 * Copyright 2021 Johnny Accot
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *      http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import logging from "logging";

const logger = logging.getLogger("@quotquot/style");
// logger.setLevel("DEBUG");

class Stylesheets {

    #sheets;

    constructor() {
        this.#sheets = {};
    }

    addSheet(url) {
        if (!url)
            throw new Error("addSheet: null argument");
        let entry = this.#sheets[url];
        if (entry) {
            if (entry.promise) {
                logger.debug(`stylesheet ${url} is loading`);
                return entry.promise.then(() => {
                    entry.refcount++;
                    logger.debug(`stylesheet ${url} is loaded (${entry.refcount})`);
                    return entry.sheet;
                });
            }
            entry.refcount++;
            logger.debug(`stylesheet ${url} is cached (${entry.refcount})`);
            return Promise.resolve(entry.sheet);
        }

        /* add entry before promise to prevent parallel calls */
        const sheet = new CSSStyleSheet();
        this.#sheets[url] = entry = { sheet, refcount: 0 };

        // github.com/WICG/construct-stylesheets/issues/119#issuecomment-588352418
        const promise = (
            fetch(url)
                .then(response => response.text())
                .then(css => sheet.replace(css))
                .then(() => {
                    entry.refcount++;
                    logger.debug(`stylesheet ${url} is fetched (${entry.refcount})`);
                    delete entry.promise;
                    return sheet;
                })
        );
        promise.catch(exc => delete this.#sheets[url]);
        entry.promise = promise;
        return promise;
    }

    async release(url) {
        const entry = this.#sheets[url];
        if (!entry || entry.promise) {
            logger.warn(`unknown stylesheet ${url}`);
            return;
        }
        entry.refcount--;
        if (entry.refcount === 0) {
            logger.debug(`removing stylesheet ${url}`);
            delete this.#sheets[url];
        }
    }
}


class AdoptedStyleManager {

    static #stylesheets = new Stylesheets();

    #root;
    #sheets;
    #callback;

    constructor() {
        this.#sheets = new Map();
    }

    setRoot(root) {
        if (!(root instanceof ShadowRoot || root instanceof Document))
            throw new Error("adopted-style must be used on document or shadow root");
        this.#root = root;
    }

    async #addSheet(url) {
        logger.debug(`adding style ${url}`);
        const style = await AdoptedStyleManager.#stylesheets.addSheet(url);
        this.#sheets.set(url, style);
    }

    async setup(root, state) {
        this.setRoot(root);
        const urls = state.get("styles");
        for (const url of urls)
            await this.#addSheet(url);
        this.#root.adoptedStyleSheets = this.#sheets.values();
        state.addObserver(this);
    }

    async cleanup(root, state) {
        state.removeObserver(this);
        for (const url of this.#sheets.keys())
            AdoptedStyleManager.#stylesheets.release(url);
        this.#root.adoptedStyleSheets = [];
        this.#sheets.clear();
        this.setRoot(null);
    }

    async onUpdate(state, oldState, newState, changed) {
        if (changed.has("styles")) {
            const urls = newState.styles;
            /* remove sheets that are not used anymore */
            for (const url of this.#sheets.keys()) {
                if (urls.includes(url)) {
                    logger.debug(`removing style ${url}`);
                    AdoptedStyleManager.#stylesheets.release(url);
                    this.#sheets.delete(url);
                }
            }
            /* define new sheets */
            for (const url of urls)
                if (!this.#sheets.has(url))
                    await this.#addSheet(url);
            this.#root.adoptedStyleSheets = this.#sheets.values();
        }
    }
}

class LinkStyleManager {

    #root;
    #links;
    #callback;

    constructor() {
        this.#links = new Map();
    }

    setRoot(root) {
        this.#root = root;
    }

    #addLink(href) {
        logger.debug(`adding style ${href}`);
        const link = document.createElement("link");
        link.rel = "stylesheet";
        link.href = href;
        this.#links.set(href, link);
        this.#root.appendChild(link);
    }

    #removeLink(href) {
        logger.debug(`removing style ${href}`);
        this.#root.removeChild(this.#links.get(href));
        this.#links.delete(href);
    }

    async setup(root, state) {
        this.setRoot(root);
        for (const url of state.get("styles"))
            this.#addLink(url);
        state.addObserver(this);
    }

    async cleanup(root, state) {
        state.removeObserver(this);
        for (const link of this.#links.values())
            link.remove();
        this.#links.clear();
        this.setRoot(null);
    }

    async onUpdate(state, oldState, newState, changed) {
        if (changed.has("styles")) {
            const newUrls = new Set(newState.styles);
            /* remove links that are not used anymore */
            for (const url of this.#links.keys())
                if (!newUrls.has(url))
                    this.#removeLink(url);
            /* define new links */
            for (const url of newUrls)
                if (!this.#links.has(url))
                    this.#addLink(url);
            /* reinsert links in correct order */ // TODO: anything smarter/faster?
            for (const url of Array.from(newState.styles).reverse())
                this.#root.prepend(this.#links.get(url));
        }
    }
}

const hasAdoptedStyleSheets = "adoptedStyleSheets" in document;
logger.debug(`adoptedStyleSheets ${hasAdoptedStyleSheets ? "" : "not "}supported`);

export const StyleManager = hasAdoptedStyleSheets ? AdoptedStyleManager : LinkStyleManager;


export class StylePlugin {

    constructor() {
        this.wrapped = new StyleManager();
    }

    get name() {
        return "style";
    }

    get requires() {
        return ["shadow", "state"];
    }

    async setup(el, plugins) {
        await this.wrapped.setup(plugins.shadow.root, plugins.state.wrapped);
    }

    async cleanup(el, plugins) {
        await this.wrapped.setup(plugins.shadow.root, plugins.state.wrapped);
    }
}

export default StylePlugin;
