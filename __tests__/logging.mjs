/* eslint no-console: off */

export default {
    getLogger: name => ({
        setLevel: level => {},
        error: (...args) => console.error(`${name}:`, ...args),
        debug: (...args) => {} // console.log(`${name}: DEBUG:`, ...args)
    })
};
