/* eslint no-return-assign: off */
/* eslint require-jsdoc: off */
/* eslint no-undef: off */
/* eslint no-console: off */

import { State } from "@quotquot/element/dist/index.mjs";
import { StyleManager } from "../index.mjs";

test("Create style-test element", async() => {

    expect("adoptedStyleSheets" in document).toBe(false);

    const HREF1 = "http://localhost/style1.css";
    const HREF2 = "http://localhost/style2.css";
    const HREF3 = "http://localhost/style3.css";

    const div = document.createElement("div");
    const shadowRoot = div.attachShadow({ mode: "open" });
    document.body.appendChild(div);

    const state = new State({ styles: [HREF1] });

    const manager = new StyleManager();
    await manager.setup(shadowRoot, state);
    expect(shadowRoot.firstElementChild.href).toBe(HREF1);

    /* add style2 */
    await state.update({ styles: [HREF1, HREF2] });
    expect(shadowRoot.firstElementChild.href).toBe(HREF1);
    expect(shadowRoot.firstElementChild.nextSibling.href).toBe(HREF2);

    /* remove style1 */
    await state.update({ styles: [HREF2] });
    expect(shadowRoot.firstElementChild.href).toBe(HREF2);
    expect(shadowRoot.firstElementChild.nextSibling).toBe(null);

    /* add style1 again, test that it is inserted becore style2 */
    await state.update({ styles: [HREF1, HREF2] });
    expect(shadowRoot.firstElementChild.href).toBe(HREF1);
    expect(shadowRoot.firstElementChild.nextSibling.href).toBe(HREF2);

    /* reverse style order and add style3 */
    await state.update({ styles: [HREF2, HREF1, HREF3] });
    expect(shadowRoot.firstElementChild.href).toBe(HREF2);
    expect(shadowRoot.firstElementChild.nextSibling.href).toBe(HREF1);
    expect(shadowRoot.firstElementChild.nextSibling.nextSibling.href).toBe(HREF3);

    /* remove both styles */
    await state.update({ styles: [] });
    expect(shadowRoot.firstElementChild).toBe(null);

    await manager.cleanup(shadowRoot, state);
    expect(document.body.innerHTML).toBe("<div></div>");
});
