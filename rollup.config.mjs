import { terser } from "rollup-plugin-terser";

export default {
    input: "index.mjs",
    output: {
        file: "dist/index.mjs",
        format: "esm"
    },
    external: ["logging"],
    plugins: [
        process.env.BUILD === "production" && terser()
    ]
};
